# import
from sklearn.metrics.pairwise import cosine_similarity, euclidean_distances
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
import numpy as np
from collections import namedtuple
import scipy.sparse

Weighting_Result = namedtuple("Weighting_Result", ["euclidean", "cosine"])


# prepare corpus
corpus = []
	
def get_docs():
    for d in range(1400):
        f = open("./cranfield/d/"+str(d+1)+".txt")
        corpus.append(f.read())
	return corpus
	
# get queries
def get_queries():
    for q in range(1, 225):
        return open("./cranfield/q/" + str(q) + ".txt").read()

# get queries		
def get_relevances(query_id):
    res = []

    for r in range(225):
        f = open("./cranfield/r/"+str(r+1)+".txt")
        res.append(list(map(int, f.read().replace('\n', ' ')[:-1].split(' '))))
        f.close()

    return res

# calculate distance
def calculate_euclidean_distance(query_vector, data_vectors):
    distances = np.array(
        euclidean_distances(query_vector, data_vectors)[0])

    sorted_distances = distances.argsort() + 1
    return sorted_distances

# calculate similarity
def calculate_cosine_similarity(query_vector, data_vectors):
    distances = np.array(
        cosine_similarity(query_vector, data_vectors)[0])

    sorted_distances = distances.argsort()[::-1] + 1
    return sorted_distances

# process weighting
def process_query_binary(query):
    docs = list(get_docs())
    docs.append(query)

    bin_vectorizer = CountVectorizer(binary=True)#
    count_array = bin_vectorizer.fit_transform(docs)

    query_vector = count_array[len(docs) - 1]  
    data_vectors = count_array[0:len(docs) - 1]  

    euclidean_distances = calculate_euclidean_distance(query_vector, data_vectors)
    cosine_similarities = calculate_cosine_similarity(query_vector, data_vectors)
    print("Binary Euclidean distance: {}".format(euclidean_distances))
    print("Binary Cosine similarity: {}".format(cosine_similarities))

    return Weighting_Result(euclidean=euclidean_distances, cosine=cosine_similarities)


def process_query_term_frequency(query):
    docs = list(get_docs())
    docs.append(query)

    tf_vectorizer = CountVectorizer()
    count_array = tf_vectorizer.fit_transform(docs)

    sums = count_array.sum(1) 
    normalized_array = count_array.multiply(1 / sums) 

    normalized_array = scipy.sparse.csr_matrix(normalized_array)

    query_vector = normalized_array[len(docs) - 1]
    data_vectors = normalized_array[0:len(docs) - 1]


    euclidean_distances = calculate_euclidean_distance(query_vector, data_vectors)
    cosine_similarities = calculate_cosine_similarity(query_vector, data_vectors)
    print("Term Frequency Euclidean distance: {}".format(euclidean_distances))
    print("Term Frequency Cosine similarity: {}".format(cosine_similarities))

    return Weighting_Result(euclidean=euclidean_distances, cosine=cosine_similarities)


def process_query_tfidf(query):
    docs = list(get_docs())
    docs.append(query)

    tfidf_vectorizer = TfidfVectorizer()
    tfidf_matrix = tfidf_vectorizer.fit_transform(docs)

    query_vector = tfidf_matrix[len(docs) - 1]
    data_vectors = tfidf_matrix[0:len(docs) - 1]

    euclidean_distances = calculate_euclidean_distance(query_vector, data_vectors)
    cosine_similarities = calculate_cosine_similarity(query_vector, data_vectors)
    print("TF-IDF Euclidean distance: {}".format(euclidean_distances))
    print("TF-IDF Cosine similarity: {}".format(cosine_similarities))

    return Weighting_Result(euclidean=euclidean_distances, cosine=cosine_similarities)

# Evaluate quality
def calculate_precision(retrieved_docs, relevant_docs):
    count = 0
    for r in range(len(relevant_docs)-1):
        for doc in retrieved_docs:
            if doc in relevant_docs[r]:
                count += 1
    #print("HOVNO HOVNO HOVNO HOVNO HOVNO HOVNO HOVNO HOVNO HOVNO HOVNO HOVNO HOVNO ")
    #print(count)
    #print(len(retrieved_docs))
    #hovno = round(count / float(len(retrieved_docs)),5)
    #print(hovno)
	
    return round(count / float(len(retrieved_docs)),5)


def calculate_recall(retrieved_docs, relevant_docs):
    count = 0
    for r in range(len(relevant_docs)-1):
        for doc in retrieved_docs:
            if doc in relevant_docs[r]:
                count += 1

    return round(count / float(len(relevant_docs)),5)


def calculate_fmeasure(precision, recall):
    if precision == 0 and recall == 0:
        return 0
    else:
        return round(2 * (precision * recall) / (precision + recall), 5)

Query_Results = namedtuple("Query_Results", ["binary_precision",
                                             "binary_recall",
                                             "tf_precision",
                                             "tf_recall",
                                             "tfidf_precision",
                                             "tfidf_recall",
                                             ])

# Process querie
def process_queries():
    euclidean_results = []
    cosine_results = []

    queries = get_queries()
    for i, query in enumerate(queries, 1):
        binary_result = process_query_binary(query)
        tf_result = process_query_term_frequency(query)
        tfidf_result = process_query_tfidf(query)

        relevant_docs = get_relevances(i)

        process_docs_count = 225

        euclidean_results.append(Query_Results(
            binary_precision=calculate_precision(binary_result.euclidean[:process_docs_count], relevant_docs),
            binary_recall=calculate_recall(binary_result.euclidean[:process_docs_count], relevant_docs),
            tf_precision=calculate_precision(tf_result.euclidean[:process_docs_count], relevant_docs),
            tf_recall=calculate_recall(tf_result.euclidean[:process_docs_count], relevant_docs),
            tfidf_precision=calculate_precision(tfidf_result.euclidean[:process_docs_count], relevant_docs),
            tfidf_recall=calculate_recall(tfidf_result.euclidean[:process_docs_count], relevant_docs)
        ))

        cosine_results.append(Query_Results(
            binary_precision=calculate_precision(binary_result.cosine[:process_docs_count], relevant_docs),
            binary_recall=calculate_recall(binary_result.cosine[:process_docs_count], relevant_docs),
            tf_precision=calculate_precision(tf_result.cosine[:process_docs_count], relevant_docs),
            tf_recall=calculate_recall(tf_result.cosine[:process_docs_count], relevant_docs),
            tfidf_precision=calculate_precision(tfidf_result.cosine[:process_docs_count], relevant_docs),
            tfidf_recall=calculate_recall(tfidf_result.cosine[:process_docs_count], relevant_docs)
        ))

    binary_euclidean_precision_sum = 0
    binary_cosine_precision_sum = 0
    binary_euclidean_recall_sum = 0
    binary_cosine_recall_sum = 0
    binary_euclidean_fmeasure_sum = 0
    binary_cosine_fmeasure_sum = 0

    tf_euclidean_precision_sum = 0
    tf_cosine_precision_sum = 0
    tf_euclidean_recall_sum = 0
    tf_cosine_recall_sum = 0
    tf_euclidean_fmeasure_sum = 0
    tf_cosine_fmeasure_sum = 0

    tfidf_euclidean_precision_sum = 0
    tfidf_cosine_precision_sum = 0
    tfidf_euclidean_recall_sum = 0
    tfidf_cosine_recall_sum = 0
    tfidf_euclidean_fmeasure_sum = 0
    tfidf_cosine_fmeasure_sum = 0

    import csv
    with open('output_data_binary.csv', mode='w') as f:
        writer_binnary = csv.writer(f, delimiter=',')

        csv_row = ['Query',
                   'Binary euclidean - recall',
                   'Binary euclidean - precision',
                   'Binary euclidean - F-measure',
                   'Binary cosine - precision',
                   'Binary cosine - recall',
                   'Binary cosine - F-measure',
                   ]
        writer_binnary.writerow(csv_row)

        for i, query in enumerate(queries, 1):
            binary_euclidean_precision = euclidean_results[i - 1].binary_precision
            binary_euclidean_recall = euclidean_results[i - 1].binary_recall
            binary_euclidean_fmeasure = calculate_fmeasure(binary_euclidean_precision, binary_euclidean_recall)
            binary_cosine_precision = cosine_results[i - 1].binary_precision
            binary_cosine_recall = cosine_results[i - 1].binary_recall
            binary_cosine_fmeasure = calculate_fmeasure(binary_cosine_precision, binary_cosine_recall)

            # sum for average
            binary_euclidean_precision_sum += binary_euclidean_precision
            binary_euclidean_recall_sum += binary_euclidean_recall
            binary_euclidean_fmeasure_sum += binary_euclidean_fmeasure
            binary_cosine_precision_sum += binary_cosine_precision				
            binary_cosine_recall_sum += binary_cosine_recall
            binary_cosine_fmeasure_sum += binary_cosine_fmeasure


            csv_row = [
                i,
                binary_euclidean_precision,
                binary_euclidean_recall,
                binary_euclidean_fmeasure,
                binary_cosine_precision,
                binary_cosine_recall,
                binary_cosine_fmeasure,
            ]
            writer_binnary.writerow(csv_row)

        csv_row = [
            "Average",
            binary_euclidean_precision_sum / len(queries),
            binary_euclidean_recall_sum / len(queries),
            binary_euclidean_fmeasure_sum / len(queries),
            binary_cosine_precision_sum / len(queries),
            binary_cosine_recall_sum / len(queries),
            binary_cosine_fmeasure_sum / len(queries),

            ]
        writer_binnary.writerow(csv_row)
			
    with open('output_data_tf.csv', mode='w') as f:
        writer_tf = csv.writer(f, delimiter=',')

        csv_row = ['Query',
                   'TF euclidean - precision',
                   'TF euclidean - recall',
                   'TF euclidean - F-measure',
                   'TF cosine - precision',
                   'TF cosine - recall',
                   'TF cosine - F-measure',
                   ]
        writer_tf.writerow(csv_row)

        for i, query in enumerate(queries, 1):
            tf_euclidean_precision = euclidean_results[i - 1].tf_precision
            tf_euclidean_recall = euclidean_results[i - 1].tf_recall
            tf_euclidean_fmeasure = calculate_fmeasure(tf_euclidean_precision, tf_euclidean_recall)
            tf_cosine_precision = cosine_results[i - 1].tf_precision
            tf_cosine_recall = cosine_results[i - 1].tf_recall
            tf_cosine_fmeasure = calculate_fmeasure(tf_cosine_precision, tf_cosine_recall)

			#sum for average
            tf_euclidean_precision_sum += tf_euclidean_precision
            tf_euclidean_recall_sum += tf_euclidean_recall
            tf_euclidean_fmeasure_sum += tf_euclidean_fmeasure
            tf_cosine_precision_sum += tf_cosine_precision
            tf_cosine_recall_sum += tf_cosine_recall
            tf_cosine_fmeasure_sum += tf_cosine_fmeasure


            csv_row = [
                i,
                tf_euclidean_precision,
                tf_euclidean_recall,
                tf_euclidean_fmeasure,
                tf_cosine_precision,
                tf_cosine_recall,
                tf_cosine_fmeasure,
            ]
            writer_tf.writerow(csv_row)

        csv_row = [
            "Average",
            tf_euclidean_precision_sum / len(queries),
            tf_euclidean_recall_sum / len(queries),
            tf_euclidean_fmeasure_sum / len(queries),
            tf_cosine_precision_sum / len(queries),
            tf_cosine_recall_sum / len(queries),
            tf_cosine_fmeasure_sum / len(queries),
            ]
        writer_tf.writerow(csv_row) 

    with open('output_data_tfidf.csv', mode='w') as f:
        writer_tfidf = csv.writer(f, delimiter=',')

        csv_row = ['Query',
                   'TF-IDF euclidean - precision',
                   'TF-IDF euclidean - recall',
                   'TF-IDF euclidean - F-measure',
                   'TF-IDF cosine - precision',
                   'TF-IDF cosine - recall',
                   'TF-IDF cosine - F-measure',
                   ]
        writer_tfidf.writerow(csv_row)

        for i, query in enumerate(queries, 1):
            tfidf_euclidean_precision = euclidean_results[i - 1].tfidf_precision
            tfidf_euclidean_recall = euclidean_results[i - 1].tfidf_recall
            tfidf_euclidean_fmeasure = calculate_fmeasure(tfidf_euclidean_precision, tfidf_euclidean_recall)
            tfidf_cosine_precision = cosine_results[i - 1].tfidf_precision
            tfidf_cosine_recall = cosine_results[i - 1].tfidf_recall
            tfidf_cosine_fmeasure = calculate_fmeasure(tfidf_cosine_precision, tfidf_cosine_recall)

			#sum for average
            tfidf_euclidean_precision_sum += tfidf_euclidean_precision
            tfidf_euclidean_recall_sum += tfidf_euclidean_recall
            tfidf_euclidean_fmeasure_sum += tfidf_euclidean_fmeasure
            tfidf_cosine_precision_sum += tfidf_cosine_precision
            tfidf_cosine_recall_sum += tfidf_cosine_recall
            tfidf_cosine_fmeasure_sum += tfidf_cosine_fmeasure


            csv_row = [
                i,
                tfidf_euclidean_precision,
                tfidf_euclidean_recall,
                tfidf_euclidean_fmeasure,
                tfidf_cosine_precision,
                tfidf_cosine_recall,
                tfidf_cosine_fmeasure,
            ]
            writer_tfidf.writerow(csv_row)

        csv_row = [
            "Average",
            tfidf_euclidean_precision_sum / len(queries),
            tfidf_euclidean_recall_sum / len(queries),
            tfidf_euclidean_fmeasure_sum / len(queries),
            tfidf_cosine_precision_sum / len(queries),
            tfidf_cosine_recall_sum / len(queries),
            tfidf_cosine_fmeasure_sum / len(queries),
            ]
        writer_tfidf.writerow(csv_row)


process_queries()