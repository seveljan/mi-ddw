import nltk
nltk.download('punkt')
nltk.download('averaged_perceptron_tagger')
nltk.download('maxent_ne_chunker')
nltk.download('words')
import numpy as np
import wikipedia

# GET text	
def get_text():
    with open('EnglishFairyTales.txt', 'r') as f:
        #text = f.read().decode('ascii','ignore') #Python
        text = f.read() #Python3
    return text
 

# POS tagging
def pos_tagging(text):
    #sentences = nltk.sent_tokenize(unicode(text, 'utf-8'))
    sentences = nltk.sent_tokenize(text)
    tokens = [nltk.word_tokenize(sent) for sent in sentences]
    pos_tagged = [nltk.pos_tag(sent) for sent in tokens]
    return pos_tagged

# NER with entity classification (using nltk.ne_chunk)
def get_ner_entities(text):  
    tokens = nltk.word_tokenize(text)
    tagged = nltk.pos_tag(tokens)
    #for tag in tagged
    #    if tag == "\u*"
    #        tagged.pop(0) #
    #print tagged[0]
    ne_chunked = nltk.ne_chunk(tagged)

    data = {}
    for entity in ne_chunked:
        if isinstance(entity, nltk.tree.Tree):
            text = " ".join([word for word, tag in entity.leaves()])
            ent = entity.label()
            if text not in data:
                data[text] = [ent, 0]
            data[text][1] += 1
        else:
            continue
    return data

# NER with custom patterns
def get_custom_ner_entities(tagged_tokens):
    tagged_entities = {}
    entity = []

    nouns = ['NN', 'NNS', 'NNP', 'NNPS']

    for tagged_sentence in tagged_tokens:
        for tagged_word in tagged_sentence:
            tag = tagged_word[1]
            if tag in nouns:
                entity.append(tagged_word)
                entity_str = " ".join([word[0] for word in entity])
                if entity_str not in tagged_entities:
                    tagged_entities[entity_str] = [entity, 0]
                tagged_entities[entity_str][1] += 1
            entity = []
    return tagged_entities


def pattern_in_wikipedia(entity):
    page = wikipedia.page(entity)
    tagged_tokens = nltk.pos_tag(nltk.word_tokenize(page.summary))

    grammar = "NP: {<DT>?<JJ>*<NN>}"
    cp = nltk.RegexpParser(grammar)
    chunked = cp.parse(tagged_tokens)

    for entity in chunked:
        if isinstance(entity, nltk.tree.Tree):
            text = " ".join([word for word, tag in entity.leaves()])
            return text
        else:
            continue


# PROCESS
def process():
    param = 50
    param_ui = 70

    text = get_text()
    tagged_tokens = pos_tagging(text)
    with open('file.txt', 'w') as f:
        print("=" * param_ui, file=f)
    with open('file.txt', 'a') as f:
        print("POS tagging:", file=f)
    with open('file.txt', 'a') as f:
        print(np.matrix(tagged_tokens[:param]), file=f)

    ner_entities = get_ner_entities(text)
    with open('file.txt', 'a') as f:
        print("=" * param_ui, file=f)
    with open('file.txt', 'a') as f:
        print("TOP entyties by NER with entity classification (using nltk.ne_chunk):", file=f)
    with open('file.txt', 'a') as f:    
        print("=" * param_ui, file=f)
    # Sort by value in entity counter (entity[1])
    sorted_entities = sorted(ner_entities.items(), key=lambda entity: entity[1][1], reverse=True)
    for elem in sorted_entities[:param]:
        with open('file.txt', 'a') as f:
            print (elem, file=f)

    custom_ner_entities = get_custom_ner_entities(tagged_tokens)
    with open('file.txt', 'a') as f:
        print("=" * param_ui, file=f)
    with open('file.txt', 'a') as f:
        print("TOP entyties by NER with custom patterns:", file=f)
    with open('file.txt', 'a') as f:
        print("=" * param_ui, file=f)
    # Sort by value in entity counter (entity[1])
    sorted_entities2 = sorted(custom_ner_entities.items(), key=lambda entity: entity[1][1], reverse=True)
    for elem in sorted_entities2[:param]:
        with open('file.txt', 'a') as f:
            print (elem, file=f)

    with open('file.txt', 'a') as f:
        print("=" * param_ui, file=f)
    with open('file.txt', 'a') as f:
        print("Tagged NLTK by NER with entity classification in the Wikipedia:", file=f)
    with open('file.txt', 'a') as f:    
        print("=" * param_ui, file=f)

    # using nltk.ne_chunk
    for named_entity in sorted_entities[:param]:
        try:
            category = pattern_in_wikipedia(named_entity[0])
            with open('file.txt', 'a') as f:
                print("{} ------> {}".format(named_entity[0], category), file=f)
        except:
            category = "Thing"
            with open('file.txt', 'a') as f:
                print("{} ------> {}".format(named_entity[0], category), file=f)
            pass

    with open('file.txt', 'a') as f:
        print("=" * param_ui, file=f)
    with open('file.txt', 'a') as f:
        print("Tagged NLTK by NER with custom patternsin the Wikipedia:", file=f)
    with open('file.txt', 'a') as f:
        print("=" * param_ui, file=f)

    # using nltk.ne_chunk
    for named_entity in sorted_entities2[:param]:
        try:
            category = pattern_in_wikipedia(named_entity[0])
            with open('file.txt', 'a') as f:
                print("{} ------> {}".format(named_entity[0], category), file=f)
        except:
            category = "Thing"
            with open('file.txt', 'a') as f:
                print("{} ------> {}".format(named_entity[0], category), file=f)
            pass 
    
process()