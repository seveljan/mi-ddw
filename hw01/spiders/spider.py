# -*- coding: utf-8 -*-
import scrapy

class vjrSpider(scrapy.Spider):
    name = 'vjrSpider'
    start_urls = ['http://www.vjrousek.cz']

    custom_settings = {  
        'ROBOTSTXT_OBEY': True,
    }

    def parse(self, response):
        categories = response.css('#fcat a')
        #categories = response.xpath('//ul[@id="menu"]//li//a[contains(@class, "a")]')
        print(categories.css('::text').extract())
        #cate = categories.css('::text').extract()
        #for x in cate:
        #    for y in x:
        #        print y
        #.decode('windows-1250')
        categories_crawl = categories.css('::attr(href)').extract()
        for category_url in categories_crawl:
            yield scrapy.Request(response.urljoin(category_url), callback=self.parse_category)
        pass

    def parse_category(self, response):
        print("Parsovani hlavni kategorie...")
        categories2 = response.css('#sm a')
        category2_urls = categories2.css('::attr(href)').extract()
        for category2_url in category2_urls:
            yield scrapy.Request(response.urljoin(category2_url), callback=self.parse_category2)

    def parse_category2(self, response):
        print("Parsovani produktu z kategorie 2. urovne...")
        category1_name = response.css('#smh::text').extract()
        cat111 = [x.encode('windows-1250') for x in category1_name]
        cat11 = ''.join(cat111)
        #for x in category1_name:
        #    for y in x:
        #        cat11=cat11+''.join(y.encode('windows-1250'))
        category2_name = response.css('h1::text').extract()
        cat1 = ''.join(category1_name)
        cat2 = ''.join(category2_name)
        
        products = response.css('div.eprod')
        
        for product in products:
            name = product.css('h2::text').extract_first()
            link = product.css('a::attr(href)').extract_first()
            price = product.xpath('//a[starts-with(@href, "javascript:xbuy")]/text()').extract_first()
            avaiability = product.css('div.pdost::text').extract_first()
            yield {
            'NAZEV': name.strip(),
            'CENA' : price.split(",")[0],
            'DOSTUPNOST': avaiability.strip(),
            'ADRESA': 'http://www.vjrousek.cz'+link,
            'KATEGORIE': cat11.strip()+' | '+cat2.strip(),
            }
